# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2025-01-28
- Add dynamic content reloader

## [1.0.13] - 2024-11-12
- G2 Brno ongoing incident - supress

## [1.0.12] - 2024-11-12
- G2 Brno ongoing incident - publish

## [1.0.11] - 2024-09-05
- Added metacentrum image on right side of header

## [1.0.10] - 2024-09-05
- G2 Brno IP address shortage notice - publish

## [1.0.9] - 2024-08-26
- G2 Brno ongoing incident - supress

## [1.0.8] - 2024-08-26
- G2 Brno ongoing incident - publish

## [1.0.7] - 2024-03-13
- Added link to G2 projects site

## [1.0.6] - 2024-03-13
- G2 Brno ongoing incident - supress

## [1.0.5] - 2024-03-11
- G2 Brno ongoing incident - publish

## [1.0.4] - 2024-03-08
- G2 Brno ongoing incident - suppress

## [1.0.3] - 2024-03-07
- G2 Brno ongoing incident - publish

## [1.0.2] - 2024-01-22
 - Update copyright to 2024
 - Add Thanos Query link to /monitoring

## [1.0.1] - 2023-12-23
 - Refactored and based on [common-welcomehub](https://gitlab.ics.muni.cz/cloud/g2/common-welcomehub)

## [1.0.0] - 2023-11-06
- Initial changelog release based on prod-ostrava welcomehub v1.3.3
